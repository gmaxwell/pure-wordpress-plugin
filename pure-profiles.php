<?php
/*
Plugin Name: Pure Profiles
Description: This plugin connects to the Atira Pure API and pulls out the profile information, which it caches in local tables. Shortcodes can be used to display profile data. This version is designed to work with Pure 4.19.0-3.
Author: Gavin Maxwell, HSS Web Team, The University of Edinburgh
Version: 2.2
Author URI: http://www.ed.ac.uk/schools-departments/humanities-soc-sci/information-for-staff/web-team
License: GPL3
*/

define('PURECLIENT_PATH', plugin_dir_path(__FILE__) );
$pure_profiles_db_version = "2.2";
$pure_profiles_current_db_version = "";

require_once(PURECLIENT_PATH.'/libraries/guzzle.phar');
require_once(PURECLIENT_PATH.'/pure-profiles-admin-menu.php');
require_once(PURECLIENT_PATH.'/models/Clipping.class.php');
require_once(PURECLIENT_PATH.'/models/Person.class.php');
require_once(PURECLIENT_PATH.'/models/Project.class.php');
require_once(PURECLIENT_PATH.'/models/Publication.class.php');
require_once(PURECLIENT_PATH.'/models/Organisation.class.php');

/* add settings link to plugins page */
function pure_profiles_settings_link($links) { 
  $settings_link = '<a href="admin.php?page=pure-profiles-menu">Settings</a>'; 
  array_unshift($links, $settings_link); 
  return $links; 
}
$plugin = plugin_basename(__FILE__); 
add_filter("plugin_action_links_$plugin", 'pure_profiles_settings_link' );

/* shortcodes */
function pure_profiles_register_shortcodes(){
	add_shortcode("pureprofiles","pure_profiles_parse");
}
function pure_profiles_parse($atts) {
	global $wpdb;
	apply_filters('debug', 'Beginning pure_profiles_parse');
		
	extract(shortcode_atts(array(
		'employeeid' => -1,
		'personuuid' => -1,
		'hide' => '',
		'display_all_info' => 'no'
	), $atts));
		
	$str = "";
	/* get profiles by employeeid or personuuid */
	if ($personuuid != -1) {
		$persons = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_person WHERE uuid='" .$personuuid."'" );
	} else if ($employeeid != -1) {
		$persons = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_person WHERE employeeid=" .$employeeid );
	} else {
		$persons = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_person" );
	}

	/* decide which sections to show */
	$hide_profile_information = FALSE;
	$hide_publications = FALSE;
	$hide_projects = FALSE;
	$hide_clippings = FALSE;
	if (trim($hide) != '') {
		if ( strpos( $hide, 'profileinformation' ) !== FALSE ) $hide_profile_information = "yes";
		if ( strpos( $hide, 'publications' ) !== FALSE ) $hide_publications = "yes";
		if ( strpos( $hide, 'projects' ) !== FALSE ) $hide_projects = "yes";
		if ( strpos( $hide, 'clippings' ) !== FALSE ) $hide_clippings = "yes";		
	} 
		
	$str.= "<div class='pure-profiles-wrapper'>";
	foreach ($persons as $p) {
		if ($p->callnamefirstname != "") {
			$the_first_name = $p->callnamefirstname;
		} else {
			$the_first_name = $p->firstname;
		}
		if ($p->callnamelastname != "") {
			$the_last_name = $p->callnamelastname;
		} else {
			$the_last_name = $p->lastname;
		}		
		$p->title = ucfirst(strtolower($p->title));
		$str.= "<span class='pure-profiles-header'>".$p->title." ".$the_first_name." ".$the_last_name."</span>";
		
		$str.= "<div class='pure-profiles-body'>";
		
		$organisations = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_organisation_association WHERE personuuid='" . $p->uuid ."'" );
		foreach ($organisations as $o) {
			$str.= "<span class='organisation-name'>".$o->organisationname."</span>";
			$staff_organisations = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_staff_organisation_association " .
													"WHERE personuuid='" . $p->uuid ."' AND organisationuuid='".$o->organisationuuid."'");
			foreach ($staff_organisations as $so) {
				$str.= "<span class='job-description'> - ".$so->jobdescription."</span><br/>";
			}	
			//$str.= $o->employmenttype."<br/>";

		}
		$str.= "</div><br/>";
		
		if ($hide_profile_information != 'yes') {
	
			$photos = explode(",", $p->photos);
			foreach ($photos as $phot) {
				if (trim($phot) != "") {
					$str.= "<img src='".plugins_url($phot, __FILE__ )."' class='pure-profiles-photo'>";
				}
			}
			$str.= "<br/>";
			
			$profile_information = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_person_information WHERE personuuid='" . $p->uuid ."'");
			foreach ($profile_information as $pi) {
				$str.= "<span class='pure-profiles-information-title'>".$pi->title."</span><br/>";
				$str.= $pi->content;
			}
		} // if !$hide_profile_information
		
		if ($display_all_info == 'yes') {
			wp_enqueue_script( 'jquery' );	
			wp_enqueue_script( 'showhide', plugins_url( 'assets/js/showhide.js', __FILE__ ) );
			wp_localize_script( 'showhide', 'showhidevars', array('pluginpath' => plugins_url( '', __FILE__) ) );
		}
		if ($hide_publications != "yes") {
			$publications = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_publication ppp,  ".
				$wpdb->prefix . "pure_profiles_publication_person pppp WHERE pppp.personuuid='" . $p->uuid . "' AND ppp.uuid=pppp.publicationuuid ORDER BY ppp.publicationyear DESC");
			$str.= "<span class='pure-profiles-publications-header'>Publications:</span>";
			$str.= "<div class='pure-profiles-publications-body'>";
			$current_year = "";
			foreach ($publications as $pub) {
				if ($current_year != $pub->publicationyear) {
					$current_year = $pub->publicationyear;
					$str.= "<div class='pure-profiles-publication-year'>".$current_year."</div>";
				}
				$str.= "<div class='pure-profiles-publication-wrapper'>";
				if ($display_all_info == 'yes') {		
					$str.= "<div class='pure-profiles-arrow pure-profiles-open'> <img src='".plugins_url('assets/img/twisty-open.png', __FILE__ )."' class='pure-profiles-twisty-open'/>&nbsp;";
					$str.= "<span class='pure-profiles-publication-title'> ";
					$str.= $pub->title;
					$str.= "</span>";					
					$str.= "</div>";			
				} else {
					$str.= "<span class='pure-profiles-publication-title'> ";
					if ($pub->dois != "") $str.= "<a href='".$pub->dois."'>";
					$str.= $pub->title;
					if (($pub->subtitle) && ($pub->subtitle != "")) {
						$str.= " : ".$pub->subtitle;
					}
					if ($pub->dois != "") $str.= "</a>";
					$str.= "</span><br/>";
				}
				$str.= "<span class='pure-profiles-publication-info'>";
				$authors_str = "";
				$authors = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_publication_person WHERE publicationuuid='" . $pub->uuid . "' AND personrole='Author'");
				$num_authors = count($authors);
				for ($i=0; $i < $num_authors; $i++) {
					$authors_str.= $authors[$i]->lastname.", ".$authors[$i]->firstname;
					if (($i == ($num_authors-2)) && ($num_authors > 1)) {
						$authors_str.= " & ";
					}elseif ($i == ($num_authors-1)) {
						$authors_str.= ". ";
					} else {
						$authors_str.= ", ";
					}
				}
				$str.= $authors_str;
				
				$publication_date = "";
				if ($pub->publicationday != '') $publication_date .= $pub->publicationday."-";
				$months = array('','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
				if ($pub->publicationmonth != "") $publication_date .= $months[(int)$pub->publicationmonth]."-";
				if ($pub->publicationyear != "") $publication_date .= $pub->publicationyear;
				$str.= " ".$publication_date." ";
				
				if ($pub->journaltitle != "") {
					$str.= " In : ".$pub->journaltitle.". ";
				}
				
				$str.= "</span><!-- end span pure-profiles-publication-info -->";
				if ($display_all_info == 'yes') {
					$keywords_str = "";
					$keywords = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_publication_keywords WHERE publicationuuid='" . $pub->uuid . "'");
					$num_keywords = count($keywords);
					for ($i=0; $i < $num_keywords; $i++) {				
						$keywords_str.= $keywords[$i]->keyword;
						if ($i == ($num_keywords-1)) {
							$keywords_str.= ". ";
						} else {
							$keywords_str.= ", ";
						}					
					}
					$organisations_str = "";
					$organisations = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_publication_organisation WHERE publicationuuid='" . $pub->uuid . "'");
					$num_organisations = count($organisations);
					for ($i=0; $i < $num_organisations; $i++) {				
						$organisations_str.= $organisations[$i]->organisationname;
						if ($i == ($num_organisations-1)) {
							$organisations_str.= ". ";
						} else {
							$organisations_str.= ", ";
						}					
					}					
					$str.= "<div class='pure-profiles-all-info-div'>";
					if (isset($pub->abstract) && ($pub->abstract != "")) $str.= $pub->abstract."<br/>";					
					$str.= "<strong>General Information</strong><br/>";
					if (isset($pub->state) && ($pub->state != "")) $str.= "State:".$pub->state."<br/>";
					$str.= "Organisations: ".$organisations_str."<br/>";
					$str.= "Authors: ".$authors_str."<br/>";
					if ($num_keywords > 0) $str.= "Keywords: (".$keywords_str.")<br/>";
					if (isset($pub->numberofpages) && ($pub->numberofpages != "")) $str.= "Number of pages: ".$pub->numberofpages."<br/>";
					if (isset($pub->journalpages) && ($pub->journalpages != "")) $str.= "Pages: ".$pub->journalpages."<br/>";
					if (isset($pub->publicationyear) && ($pub->publicationyear != "")) {
						$str.= "Publication Date: ";
						if (isset($pub->publicationday) && ($pub->publicationday != "")) $str.= $pub->publicationday." ";
						if (isset($pub->publicationmonth) && ($pub->publicationmonth != "")) $str.= $months[(int)$pub->publicationmonth]." ";
						$str.= $pub->publicationyear."<br/>";
					}
				//	$str.= "Peer-reviewed: Yes<br/>";
					$str.= "<strong>Publication Information</strong><br/>";
					if (isset($pub->typeclassification) && ($pub->typeclassification != "")) $str.= "Category: ".$pub->typeclassification."<br/>";
					if (isset($pub->journaltitle) && ($pub->journaltitle != "")) $str.= "Journal: ".$pub->journaltitle."<br/>";
					if (isset($pub->volume) && ($pub->volume != "")) $str.= "Volume: ".$pub->volume."<br/>";
					if (isset($pub->journalnumber) && ($pub->journalnumber != "")) $str.= "Issue number: ".$pub->journalnumber."<br/>";
					if (isset($pub->journalissn) && ($pub->journalissn != "")) $str.= "ISSN: ".$pub->journalissn."<br/>";
					if (isset($pub->originallanguage) && ($pub->originallanguage != "")) $str.= "Original Language: ".$pub->originallanguage."<br/>";
					if (isset($pub->dois) && ($pub->dois != "")) {
						$str.= "DOIs: ";
						$str.= "<a href='".$pub->dois."'>".str_replace("http://dx.doi.org/","",$pub->dois)."</a><br/>";
					}
					$str.= "</div>";
				}
				$str.= "</div><!-- end pure-profiles-publication-wrapper -->";
			}
			$str.= "</div><br/>";
		} // end if !$hide_publications
		
		if ($hide_projects != "yes") {
			$projects = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_project ppp, " . 
				$wpdb->prefix . "pure_profiles_project_person pppp WHERE pppp.personuuid='" . $p->uuid . "' AND ppp.uuid=pppp.projectuuid");
			if (count($projects) > 0) {
				$str.= "<span class='pure-profiles-projects-header'>Projects:</span>";
				$str.= "<div class='pure-profiles-projects-body'>";
				foreach ($projects as $proj) {
					$str.= "<span class='pure-profiles-project-title'>".$proj->title."</span><br/>";
					$project_persons = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_project_person WHERE projectuuid='" . $proj->uuid . "'");
					for ($pp = 0; $pp < count($project_persons); $pp++) {
						$first_name = ($project_persons[$pp]->callnamefirstname != "") ? $project_persons[$pp]->callnamefirstname : $project_persons[$pp]->firstname;
						$last_name = ($project_persons[$pp]->callnamelastname != "") ? $project_persons[$pp]->callnamelastname : $project_persons[$pp]->lastname;
						$str.= $first_name.", ".$last_name." (".$project_persons[$pp]->rolename.")<br>";
					}
					$start_date = date("j/m/Y", strtotime($proj->startdate));
					$end_date = date("j/m/Y", strtotime($proj->finishdate));
					$str.= "Period: <span class='pure-profiles-project-period'>".$start_date."<abbr class='pure-profiles-project-period-arrow' title='to'>&ndash;</abbr>".$end_date."</span><br/>";
					$str.= "Funding Organisation: <span class='pure-profiles-project-fundingorganisation'>".$proj->fundingorganisation."</span><br/>";
				}
				$str.= "</div><br/>";
			}
		} // end if !$hide_projects
		
		if ($hide_clippings != 'yes') {
			$clippings = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "pure_profiles_clipping ppc, " . 
				$wpdb->prefix . "pure_profiles_clipping_person ppcp WHERE ppcp.personuuid='" . $p->uuid . "' AND ppc.clippinguuid=ppcp.clippinguuid");
			if (count($clippings) > 0) {
				$str.= "<span class='pure-profiles-clippings-header'>Research press coverage:</span>";
				$str.= "<div class='pure-profiles-clippings-body'>";
				foreach ($clippings as $clip) {
					$str.= "<span class='pure-profiles-clipping-title'>".$clip->title."</span><br/>";
					$clip_date = date("j/m/Y", strtotime($clip->date));
					$str.= "<span class='pure-profiles-clipping-info'>".$clip_date."</span><br/>";
				}
				$str.= "</div><br/>";
			}
		} // end if !$hide_clippings
	}
	$str.= '</div>';
	apply_filters( 'debug', 'Exiting pure_profiles_parse' );
	
	return $str;
}
add_action( 'init', 'pure_profiles_register_shortcodes' );

/* shortcode button for TinyMCE */
add_action( 'init', 'pure_profiles_shortcode_button_init' );
function pure_profiles_shortcode_button_init() {

	//Abort early if the user will never see TinyMCE
	if ( ! current_user_can( 'edit_posts' ) && ! current_user_can( 'edit_pages' ) && get_user_option( 'rich_editing' ) == 'true')
		return;

	add_filter( 'mce_external_plugins', 'pure_profiles_register_tinymce_plugin' ); 
	add_filter( 'mce_buttons', 'pure_profiles_add_tinymce_button' );
}

/* This callback registers the plug-in */
function pure_profiles_register_tinymce_plugin( $plugin_array ) {
	$plugin_array['pure_profiles_button'] = plugins_url( '/libraries/tinymce/pureprofiles.js', __FILE__ );
	return $plugin_array;
}

/* This callback adds the button to the toolbar */
function pure_profiles_add_tinymce_button( $buttons ) {
	$buttons[] = 'pure_profiles_button';
	return $buttons;
}


/* add CSS */
$appearance_options = get_option( 'pure_profiles_appearance_options' );
$enable_styles = htmlentities( $appearance_options['pure_profiles_enable_styles_settings'], ENT_QUOTES );
if ( $enable_styles == 'yes' ) {
	add_action( 'init', 'pure_profiles_register_style' );
	function pure_profiles_register_style(){
		wp_register_style( 'pure_profile_styles', plugins_url( '/styles.css', __FILE__ ) );
	}
	add_action( 'wp_enqueue_scripts', 'pure_profiles_enqueue_style' );
	function pure_profiles_enqueue_style() {
		wp_enqueue_style( 'pure_profile_styles' );
	}
}
