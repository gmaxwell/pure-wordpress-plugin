jQuery(document).ready(function($) {
	var mypath = showhidevars.pluginpath;
	$('.pure-profiles-all-info-div').toggle(); 	// close items if javascript detected
	$('.pure-profiles-arrow').removeClass('pure-profiles-open').addClass('pure-profiles-closed').find('img').attr('src', mypath + '/assets/img/twisty.png');

	$('.pure-profiles-arrow').click(function() {
		if ($(this).hasClass('pure-profiles-open')) {
			$(this).removeClass('pure-profiles-open').addClass('pure-profiles-closed');
			$(this).find('img').attr('src', mypath + '/assets/img/twisty.png');
			//$(this).parent().find('.pure-profiles-publication-info').toggle('fast', function() {});					
			$(this).parent().find('.pure-profiles-all-info-div').toggle('fast', function() {});
		} else if ($(this).hasClass('pure-profiles-closed')) {
			$(this).removeClass('pure-profiles-closed').addClass('pure-profiles-open');
			$(this).find('img').attr('src', mypath + '/assets/img/twisty-open.png');
			//$(this).parent().find('.pure-profiles-publication-info').toggle('fast', function() {});			
			$(this).parent().find('.pure-profiles-all-info-div').toggle('fast', function() {});
		}
	});
	
	$('.pure-profiles-arrow').hover(function () {
		if ($(this).hasClass('pure-profiles-open')) {
			$(this).find('img').attr('src', mypath + '/assets/img/twisty-active-open.png');
		} else if ($(this).hasClass('pure-profiles-closed')) {
			$(this).find('img').attr('src', mypath + '/assets/img/twisty-active.png');
		}	
    }, function () {
		if ($(this).hasClass('pure-profiles-open')) {
			$(this).find('img').attr('src', mypath + '/assets/img/twisty-open.png');
		} else if ($(this).hasClass('pure-profiles-closed')) {
			$(this).find('img').attr('src', mypath + '/assets/img/twisty.png');
		}    
	});	
	
});
