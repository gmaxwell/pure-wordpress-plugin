<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}

add_action( 'admin_menu', 'pure_profiles_menu' );
function pure_profiles_menu() {
	add_menu_page('Pure Profiles', 'Pure Profiles', 'manage_options', 'pure-profiles-menu', 'pure_profiles_connection_options', 'dashicons-id-alt');
	add_submenu_page( 'pure-profiles-menu', 'Pure Profiles - Connection Details', 'Connection Details', 'manage_options', 'pure-profiles-menu', 'pure_profiles_connection_options' ); 
	add_submenu_page( 'pure-profiles-menu', 'Pure Profiles - Parameters', 'Parameters', 'manage_options', 'pure-profiles-menu-parameters', 'pure_profiles_parameters_options' ); 		
	add_submenu_page( 'pure-profiles-menu', 'Pure Profiles - Cron', 'Cron', 'manage_options', 'pure-profiles-menu-cron', 'pure_profiles_cron_options' ); 	
	add_submenu_page( 'pure-profiles-menu', 'Pure Profiles - Appearance', 'Appearance', 'manage_options', 'pure-profiles-menu-appearance', 'pure_profiles_appearance_options' ); 
}

function pure_profiles_connection_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Connection options updated.</p></div>';
	}
	$pure_profiles_options = array(
		'pure_profiles_host_settings'=> '', 
		'pure_profiles_username_settings' => '', 
		'pure_profiles_password_settings' =>''		
		);
	add_option('pure_profiles_connection_options', $pure_profiles_options);
?>	
	<div class="wrap">
	<h2>Pure Profiles</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_profiles_connection_options'); ?>
		<?php do_settings_sections('pure_profiles_connection'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" style="margin-top:30px;" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_profiles_connection_options'); ?>
	</form>	
	</div>
<?php						
}

function pure_profiles_parameters_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Parameters updated.</p></div>';
	}
	$pure_profiles_options = array(
		'pure_profiles_person_ids_settings' => '',
		'pure_profiles_person_uuids_settings' => '',		
		'pure_profiles_org_uuids_settings' => ''
		);

?>	
	<div class="wrap">
	<h2>Pure Profiles</h2>
<?php
	add_option('pure_profiles_parameters_options', $pure_profiles_options);

	$options = get_option('pure_profiles_parameters_options');
?>	
	<form action="options.php" method="post">				
		<?php settings_fields('pure_profiles_parameters_options'); ?>
		<?php do_settings_sections('pure_profiles_parameters'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" style="margin-top:30px;" value="Save Changes" />
		<p></p>
	</form>	
	</div>
<?php		
}
 
$cron_key = uniqid();
function pure_profiles_cron_options() {
	global $cron_key;
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Cron options updated.</p></div>';
	}
	$pure_profiles_options = array(	
		'pure_profiles_cron_key_settings' => $cron_key
	);
	add_option('pure_profiles_cron_options', $pure_profiles_options);
?>	
	<div class="wrap">
	<h2>Pure Profiles</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_profiles_cron_options'); ?>
		<?php do_settings_sections('pure_profiles_cron'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" style="margin-top:30px;" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_profiles_cron_options'); ?>
	</form>	
	</div>
<?php		
}

function pure_profiles_appearance_options() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	if ( isset($_GET['settings-updated']) ) {
		echo '<div class="updated"><p>Appearance options updated.</p></div>';
	}
	$pure_profiles_options = array(
		'pure_profiles_enable_styles_settings' =>'no'
		);
	add_option('pure_profiles_appearance_options', $pure_profiles_options);
?>	
	<div class="wrap">
	<h2>Pure Profiles</h2>
	<form action="options.php" method="post">				
		<?php settings_fields('pure_profiles_appearance_options'); ?>
		<?php do_settings_sections('pure_profiles_appearance'); ?>
		<input name="cat_submit" type="submit" id="submit" class="button-primary" style="margin-top:30px;" value="Save Changes" />
		<p></p>
		<?php $options = get_option('pure_profiles_appearance_options'); ?>
	</form>	
	</div>
<?php		
}

add_action('admin_init', 'pure_profiles_admin_init');
function pure_profiles_admin_init(){
	global $pure_profiles_db_version, $pure_profiles_current_db_version;
	/* if tables do not exist, then create them (this is in admin init so that it works on multisite) */
	if (! get_option('pure_profiles_db_version')) {
		require_once(PURECLIENT_PATH.'/install.php');	
		pure_profiles_install();
	}
	// initialise options array
	$connection_options = get_option('pure_profiles_connection_options');
	$parameters_options = get_option('pure_profiles_parameters_options');
	$cron_options 		= get_option('pure_profiles_cron_options');
	$appearance_options = get_option('pure_profiles_appearance_options');
	
	/* check to see if any db updates need to be made */
	$pure_profiles_current_db_version = get_option('pure_profiles_db_version');
	if (version_compare($pure_profiles_current_db_version, $pure_profiles_db_version, '<')) {
		require_once(PURECLIENT_PATH.'/update.php');
		pure_profiles_version_update();
	}

	register_setting( 'pure_profiles_connection_options', 'pure_profiles_connection_options', 'pure_profiles_connection_options_validate' );
	register_setting( 'pure_profiles_parameters_options', 'pure_profiles_parameters_options', 'pure_profiles_parameters_options_validate' );
	register_setting( 'pure_profiles_cron_options', 'pure_profiles_cron_options', 'pure_profiles_cron_options_validate' );
	register_setting( 'pure_profiles_appearance_options', 'pure_profiles_appearance_options', 'pure_profiles_appearance_options_validate' );
	add_settings_section('pure_profiles_connection', '', 'pure_profiles_connection_section_text', 'pure_profiles_connection', 'pure_profiles_connection' );
	add_settings_field('pure_profiles_hostname', 'Hostname', 'pure_profiles_host_settings', 'pure_profiles_connection', 'pure_profiles_connection' );
	add_settings_field('pure_profiles_username', 'Username', 'pure_profiles_username_settings', 'pure_profiles_connection', 'pure_profiles_connection' );
	add_settings_field('pure_profiles_password', 'Password', 'pure_profiles_password_settings', 'pure_profiles_connection', 'pure_profiles_connection' );
	add_settings_section('pure_profiles_parameters', '', 'pure_profiles_parameters_section_text', 'pure_profiles_parameters', 'pure_profiles_parameters');
	add_settings_field('pure_profiles_person_uuids', 'Pure Person UUIDs', 'pure_profiles_person_uuids_settings', 'pure_profiles_parameters', 'pure_profiles_parameters' );
	add_settings_field('pure_profiles_person_ids', 'Employee IDs', 'pure_profiles_person_ids_settings', 'pure_profiles_parameters', 'pure_profiles_parameters' );	
	add_settings_field('pure_profiles_org_uuids', 'Pure Organisation UUIDs', 'pure_profiles_org_uuids_settings', 'pure_profiles_parameters', 'pure_profiles_parameters' );
	add_settings_section('pure_profiles_cron', '', 'pure_profiles_cron_section_text', 'pure_profiles_cron', 'pure_profiles_cron' );
	add_settings_field('pure_profiles_cron_key', 'Key for Cron', 'pure_profiles_cron_key_settings', 'pure_profiles_cron', 'pure_profiles_cron' );
	add_settings_section('pure_profiles_appearance', '', 'pure_profiles_appearance_section_text', 'pure_profiles_appearance', 'pure_profiles_appearance' );
	add_settings_field('pure_profiles_enable_styles', 'Use Default Stylesheet', 'pure_profiles_enable_styles_settings', 'pure_profiles_appearance', 'pure_profiles_appearance' );
	
}

function pure_profiles_connection_section_text() {
	echo '<h3>Connection Details</h3><p>Set the connection details for your Pure installation. </p>';
}

function pure_profiles_parameters_section_text() {
	echo '<h3>Parameters</h3><p>Specify the data you wish to retrieve from Pure. You can list Employee IDs, Pure UUIDs, or a mixture of both. Specifying 
	an organisation\'s Pure UUID will retrieve all of the people associated with that organisation.</p>
		<p>All formats should be in a comma separated list.</p>
		<p>Note: Pure UUIDs can be found in the URLs in the Edinburgh Research Explorer, within the brackets.</p>';
}

function pure_profiles_cron_section_text() {
	global $cron_key;
	$options = get_option( 'pure_profiles_cron_options' );
	$cron_key = ($options['pure_profiles_cron_key_settings'] != "") ? $options['pure_profiles_cron_key_settings'] : $cron_key;	
	$cron_link = plugins_url( 'cron.php?key='.$cron_key , __FILE__ ) ;
	
	echo '<h3>Cron</h3><p>You should set up a cron to update the local cache. For example, to update every day at 4am:</p>
		<code>0 4 * * * curl '.$cron_link.' >/dev/null 2>&1</code>';
	echo '<p>It is important that the script is <strong>not</strong> called during the day or between midnight and 3am.</p>';
}

function pure_profiles_appearance_section_text() {
	echo '<h3>Appearance</h3><p>To use your own styles, untick the Use Default Stylesheet option, and implement the styles in your theme stylesheet.</p>';
}

function pure_profiles_host_settings() {
	$options = get_option( 'pure_profiles_connection_options' );
	$value = htmlentities ( $options['pure_profiles_host_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_profiles_host_settings' name='pure_profiles_connection_options[pure_profiles_host_settings]' size='50' type='text' value='{$value}' placeholder='e.g. http://www-beta.pure.ed.ac.uk' />";
}
function pure_profiles_username_settings() {
	$options = get_option('pure_profiles_connection_options');
	$value = htmlentities ( $options['pure_profiles_username_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_profiles_username_settings' name='pure_profiles_connection_options[pure_profiles_username_settings]' size='50' type='text' value='{$value}' placeholder='Enter username' />";
}
function pure_profiles_password_settings() {
	$options = get_option('pure_profiles_connection_options');
	$value = htmlentities ( $options['pure_profiles_password_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<input id='pure_profiles_password_settings' name='pure_profiles_connection_options[pure_profiles_password_settings]' size='50' type='password' value='{$value}' placeholder='Enter password' />";
}
function pure_profiles_person_uuids_settings() {
	$options = get_option('pure_profiles_parameters_options');
	$value = htmlentities ( $options['pure_profiles_person_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_profiles_person_uuids_settings' name='pure_profiles_parameters_options[pure_profiles_person_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_profiles_person_ids_settings() {
	$options = get_option('pure_profiles_parameters_options');
	$value = htmlentities ( $options['pure_profiles_person_ids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_profiles_person_ids_settings' name='pure_profiles_parameters_options[pure_profiles_person_ids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_profiles_org_uuids_settings() {
	$options = get_option('pure_profiles_parameters_options');
	$value = htmlentities ( $options['pure_profiles_org_uuids_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = '';
	}
	echo "<textarea id='pure_profiles_org_uuids_settings' name='pure_profiles_parameters_options[pure_profiles_org_uuids_settings]' rows='4' cols='50'>{$value}</textarea>";
}
function pure_profiles_cron_key_settings() {
	global $cron_key;
	$options = get_option('pure_profiles_cron_options');
	$value = htmlentities ( $options['pure_profiles_cron_key_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = $cron_key;
	}
	echo "<input id='pure_profiles_cron_key_settings' name='pure_profiles_cron_options[pure_profiles_cron_key_settings]' size='50' type='text' value='{$value}' />";
}
function pure_profiles_enable_styles_settings() {
	$options = get_option('pure_profiles_appearance_options');
	$value = htmlentities ( $options['pure_profiles_enable_styles_settings'], ENT_QUOTES );
	if ( !$value ) {
		$value = 'no';
	}
	$checked = ($value=='yes') ? " checked" : "";
	echo "<input id='pure_profiles_enable_styles_settings' name='pure_profiles_appearance_options[pure_profiles_enable_styles_settings]' type='checkbox' value='yes' {$checked} />";
}

function pure_profiles_connection_options_validate($input) {
	$options = get_option( 'pure_profiles_connection_options' );
	$options['pure_profiles_host_settings'] = trim($input['pure_profiles_host_settings']);
	$options['pure_profiles_username_settings'] = trim($input['pure_profiles_username_settings']);
	$options['pure_profiles_password_settings'] = trim($input['pure_profiles_password_settings']);	
	return $options;
}
function pure_profiles_parameters_options_validate($input) {
	$options = get_option( 'pure_profiles_parameters_options' );
	$options['pure_profiles_person_uuids_settings'] = trim($input['pure_profiles_person_uuids_settings']); 	
	$options['pure_profiles_person_ids_settings'] = trim($input['pure_profiles_person_ids_settings']); 
	$options['pure_profiles_org_uuids_settings'] = trim($input['pure_profiles_org_uuids_settings']); 			
	return $options;
}
function pure_profiles_cron_options_validate($input) {
	$options = get_option( 'pure_profiles_cron_options' );
	$options['pure_profiles_cron_key_settings'] = trim($input['pure_profiles_cron_key_settings']);	
	return $options;
}
function pure_profiles_appearance_options_validate($input) {
	$options = get_option( 'pure_profiles_appearance_options' );
	$options['pure_profiles_enable_styles_settings'] = trim($input['pure_profiles_enable_styles_settings']);		
	return $options;
}