<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}

function pure_profiles_version_update() {
	global $pure_profiles_current_db_version, $pure_profiles_db_version, $wpdb;
	
  	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	
	if (version_compare($pure_profiles_current_db_version, '1.8', '<')) {
		exit(); // do nothing, earlier versions should be uninstalled 						
	}

	//if (floatval($pure_profiles_current_db_version) < floatval(1.9)) { 
	if (version_compare($pure_profiles_current_db_version, '1.9', '<')) {
		// create documents table
		$table_name = $wpdb->prefix . "pure_profiles_publication_document";  
		$sql = "CREATE TABLE $table_name (
		  id int(9) NOT NULL AUTO_INCREMENT,
		  publicationuuid varchar(40),
		  url VARCHAR(255),
		  retrieved varchar(30),
		  UNIQUE KEY id (id)
			);";
		dbDelta( $sql );	
		
		// update to 1.9
		update_option( "pure_profiles_db_version", '1.9' );
						
	}
	
	if (version_compare($pure_profiles_current_db_version, '1.10', '<')) {
		
		/* add portal URLs */
		$table_name = $wpdb->prefix . "pure_profiles_person";  					
		$sql = "ALTER TABLE $table_name 
			ADD `portalurl` VARCHAR(255) NOT NULL AFTER `employeeid`;";
			echo $sql;
		$wpdb->query($sql);

		$table_name = $wpdb->prefix . "pure_profiles_organisation";  					
		$sql = "ALTER TABLE $table_name 
			ADD `portalurl` VARCHAR(255) NOT NULL AFTER `organisationshortname`;";
		$wpdb->query($sql);

		
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name 
			ADD `portalurl` VARCHAR(255) NOT NULL AFTER `title`;";
		$wpdb->query($sql);
		
		$table_name = $wpdb->prefix . "pure_profiles_project";  					
		$sql = "ALTER TABLE $table_name 
			ADD `portalurl` VARCHAR(255) NOT NULL AFTER `title`;";
		$wpdb->query($sql);
		
		/* add ISBNs */
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name 
			ADD `isbns` VARCHAR(255) NOT NULL AFTER `dois`;";
		$wpdb->query($sql);
		
		/* add Bibliographical Note */
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name 
			ADD `bibliographicalnote` TEXT NOT NULL AFTER `journalissn`;";
		$wpdb->query($sql);
	
		
		// update to 1.10
		update_option( "pure_profiles_db_version", '1.10' );

	}
	

	if (version_compare($pure_profiles_current_db_version, '1.11', '<')) {
	
		$table_name = $wpdb->prefix . "pure_profiles_project";  					
		$sql = "ALTER TABLE $table_name 
			ADD `laymansdescription` TEXT NOT NULL AFTER `description`;";
		$wpdb->query($sql);
		
		$table_name = $wpdb->prefix . "pure_profiles_project_publication_association";  
		$sql = "CREATE TABLE $table_name (		
			id int(11) NOT NULL AUTO_INCREMENT,
			project_uuid varchar(40) NOT NULL,
			project_title varchar(255) NOT NULL,
			publication_uuid varchar(40) NOT NULL,
			publication_title varchar(255) NOT NULL,
			retrieved varchar(30) NOT NULL,
			UNIQUE KEY id (id)
		);"; 
		dbDelta( $sql );	
	
		// update to 1.11
		update_option( "pure_profiles_db_version", '1.11' );
						
	}
	
	if (version_compare($pure_profiles_current_db_version, '1.12', '<')) {

		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name CHANGE `isbns` `printisbns` VARCHAR( 255 ) NOT NULL ";
		$wpdb->query($sql);
			
		$sql = "ALTER TABLE $table_name ADD `eisbns` VARCHAR( 255 ) NOT NULL AFTER `printisbns`";
		$wpdb->query($sql);
			
		// update to 1.12
		update_option( "pure_profiles_db_version", '1.12' );
						
	}
	
	if (version_compare($pure_profiles_current_db_version, '1.13', '<')) {
	
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name ADD `subtitle` TEXT NULL DEFAULT NULL AFTER `title`";
		$wpdb->query($sql);
	
		$table_name = $wpdb->prefix . "pure_profiles_publication_publication_association";
		$sql = "CREATE TABLE $table_name (
			id int(11) NOT NULL AUTO_INCREMENT,
			pub1_uuid varchar(40) NOT NULL,
			pub1_title text NOT NULL,
			pub2_uuid varchar(40) NOT NULL,
			pub2_title text NOT NULL,
			retrieved varchar(30) NOT NULL,
			UNIQUE KEY (`id`)
			);";
		dbDelta( $sql );		
	
		// update to 1.13
		update_option( "pure_profiles_db_version", '1.13' );	
	}
		
	
	if (version_compare($pure_profiles_current_db_version, '1.14', '<')) {
	
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name ADD `vancouver` TEXT NULL DEFAULT NULL AFTER `bibliographicalnote`";
		$wpdb->query($sql);	
		
		// update to 1.14
		update_option( "pure_profiles_db_version", '1.14' );	
	}
	
	if (version_compare($pure_profiles_current_db_version, '1.15', '<')) {
	
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name ADD `harvard` TEXT NULL DEFAULT NULL AFTER `vancouver`";
		$wpdb->query($sql);	
	
		// update to 1.15
		update_option( "pure_profiles_db_version", '1.15' );	
	}	
	
	if (version_compare($pure_profiles_current_db_version, '1.16', '<')) {
		
		$table_name = $wpdb->prefix . "pure_profiles_publication";  					
		$sql = "ALTER TABLE $table_name ADD `peerreview` VARCHAR(10) NULL DEFAULT NULL AFTER `harvard`";
		$wpdb->query($sql);				

		$table_name = $wpdb->prefix . "pure_profiles_project";  					
		$sql = "ALTER TABLE $table_name ADD `parentproject` VARCHAR(40) NULL DEFAULT NULL AFTER `associatedpublications`";
		$wpdb->query($sql);	
		$sql = "ALTER TABLE $table_name DROP `associatedpublications`";
		$wpdb->query($sql);	
		
		$table_name = $wpdb->prefix . "pure_profiles_project_organisation";  					
		$sql = "ALTER TABLE $table_name CHANGE `uuid` `projectuuid` VARCHAR( 40 ) NULL DEFAULT NULL";
		$wpdb->query($sql);
		$sql = "ALTER TABLE $table_name ADD `title` TEXT NULL DEFAULT NULL AFTER projectuuid, ADD `organisationuuid` VARCHAR( 40 ) NULL DEFAULT NULL AFTER title";
		$wpdb->query($sql);
		$sql = "ALTER TABLE $table_name ADD `organisationshortname` VARCHAR( 255 ) AFTER `organisationname`, ADD `orgtypeclassification` VARCHAR( 255 ) AFTER `organisationshortname`, ADD `orgtypeclassificationid` VARCHAR( 255 ) AFTER `orgtypeclassification`";
		$wpdb->query($sql);
		$sql = "ALTER TABLE $table_name DROP INDEX uuid";
		$wpdb->query($sql);
		
		$table_name = $wpdb->prefix . "pure_profiles_project_project_association";
		$sql = "CREATE TABLE $table_name (
		  `id` int(11) NOT NULL AUTO_INCREMENT,
		  `proj1_uuid` varchar(40) NOT NULL,
		  `proj1_title` text NOT NULL,
		  `proj2_uuid` varchar(40) NOT NULL,
		  `proj2_title` text NOT NULL,
		  `retrieved` varchar(30) NOT NULL,
		  PRIMARY KEY (`id`)
		);";		
		dbDelta( $sql );		
	
		// update to 1.16
		update_option( "pure_profiles_db_version", '1.16' );
	}	
	
}