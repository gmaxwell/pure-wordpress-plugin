<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}
require_once(PURECLIENT_PATH.'/libraries/guzzle.phar');
use Guzzle\Http\Client;

$connection_options = get_option('pure_profiles_connection_options');
if ($connection_options == NULL) return;
$host = $connection_options['pure_profiles_host_settings'];
$username = $connection_options['pure_profiles_username_settings'];
$password = $connection_options['pure_profiles_password_settings'];
$client = new Client($host, array(
	'request.options' => array(
		'auth'    => array($username, $password, 'Basic|Digest|NTLM|Any'),

	),
	'curl.options' => array(
		'curl.CURLOPT_SSL_VERIFYPEER' => false,
		'curl.CURLOPT_CERTINFO'       => false	
	),
	'ssl.certificate_authority' => false
));

class Organisation {

	public static function get_pure_organisations($clear_database=0) {
		global $wpdb, $client, $username, $password;
		$parameters_options = get_option('pure_profiles_parameters_options');
		$str_org_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_profiles_org_uuids_settings'])); // remove all whitespace

		$request = $client->get('/ws/rest/organisation?uuids.uuid='.$str_org_uuids.'&rendering=xml_long');
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
				
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
		$core = $xml->children($namespaces['core']);	
		$result_count = $core->count;
		if ($result_count == 0) return;

		
		/* if query is successful, and flag is set, clear out existing entries */
		if (($result_count > 0) && ($clear_database == 1) ) {
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_organisation";
			$wpdb->query($truncate_sql);
		} 
		
		$retrieved = date("F j, Y, g:i a"); 
		for ($i=0; $i < $core->count; $i++) {
			$organisation = $core->result->content[$i]->children($namespaces['stab']);
			$o_core = $core->result->content[$i]->children($namespaces['core']);		
			$attributes = $core->result->content[$i]->attributes();
			$uuid = (string)$attributes['uuid'];	
			$organisation_id = (string)$organisation->organisationId;
			$name = $organisation->name->children($namespaces['core']);
			$org_name = $name->localizedString;
			if ($organisation->shortName) {
				$short_name = $organisation->shortName->children($namespaces['core']);
				$org_short_name = (string)$short_name->localizedString;
			} else {
				$org_short_name = "";
			}
			$portal_url = $o_core->portalUrl;
			$org_website = "";
			if ($organisation->website) {
				$website = $organisation->website->children($namespaces['core']);
				$org_website = $website->localizedString;
			}
			$phone = $organisation->phone;
			$email = $organisation->email;			
			
			/* insert organisation into table */
			$table_name = $wpdb->prefix . "pure_profiles_organisation";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'uuid' => $uuid, 
				'organisationid' => $organisation_id, 				
				'organisationname' => $org_name, 
				'organisationshortname' => $org_short_name, 
				'portalurl' => $portal_url,
				'website' => $org_website,
				'phone' => $phone,
				'email' => $email,
				'retrieved' => $retrieved
			));
			
			/* get all profile information */
			if ($organisation->profileInformation) {
				$profile_information = $organisation->profileInformation->children($namespaces['extensions-core']);
				$num_profile_information = count($profile_information->customField);
				for ($pi=0; $pi < $num_profile_information; $pi++) {
					$info_title = $profile_information->customField[$pi]->typeClassification->children($namespaces['core']);
					$the_info_title = $info_title->term->localizedString;
					$info_content = $profile_information->customField[$pi]->value->children($namespaces['core']);
					$the_info_content = ($info_content->localizedString) ? $info_content->localizedString : "";
							
					/* insert profile information into table */
					$table_name = $wpdb->prefix . "pure_profiles_organisation_information";
					$rows_affected = $wpdb->insert( $table_name, array( 
						'organisationuuid' => $uuid, 
						'title' => $the_info_title, 
						'content' => $the_info_content, 
						'retrieved' => $retrieved
					));				
				}
			}
			Publication::get_publications("Organisation", $uuid);
			Project::get_projects("Organisation", $uuid);
		}
		
	}

}

