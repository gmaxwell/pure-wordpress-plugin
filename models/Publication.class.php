<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}
require_once(PURECLIENT_PATH.'/libraries/guzzle.phar');
use Guzzle\Http\Client;
		
class Publication {

	public static function get_publications($type="Person", $uuid) {
		global $wpdb, $client, $username, $password;
								
		$publication_uuids = Publication::get_pure_publications_uuids($type, $uuid); // get array of publication UUIDs
		if (count($publication_uuids) == 0 ) return;

		/* get publications by UUID, in batches of 20 */
		$batch = array();		
		$num_publications = count($publication_uuids);
		for ($i=0; $i< $num_publications; $i++ ) {
			if (trim($publication_uuids[$i]) != "") {
				$batch[] = $publication_uuids[$i];
			}
		
			if (($i>0) && ((($i+1) % 20) == 0) ) { 

				// make call
				Publication::get_pure_publications_batch($batch);
				
				// make a new batch
				$batch = array();
			}
				
		}
		/* do remaining batch */
		if (count($batch) > 0) {
			Publication::get_pure_publications_batch($batch);
		}		
		
	}
	
	public static function get_pure_publications_uuids($type, $uuid) {
		global $wpdb, $client, $username, $password;

		/* get publication count */
		$request = $client->get('/ws/rest/publication?rendering=xml_short&associated'.$type.'Uuids.uuid='.$uuid.'&window.size=1');
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
				
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
		$core = $xml->children($namespaces['core']);	
		$result_count = $core->count;
		if ($result_count == 0) return;		
		$publication_uuids = array();
		

		/* get publication UUIDs, in batches of 20 */
		$batch_size = 20;
		for ($i =0; $i < $result_count; $i = $i+$batch_size) {
		
			$request = $client->get('/ws/rest/publication?rendering=xml_short&associated'.$type.'Uuids.uuid='.$uuid.'&window.size='.$batch_size.'&window.offset='.$i);
			$request->setAuth($username, $password);
			try {
				$response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}
				
			$xml = $response->xml();
			$namespaces = $xml->getNamespaces(true);
			$core = $xml->children($namespaces['core']);	
			$num_batch_results = count($core->result->content);
			for ($j=0; $j < $num_batch_results; $j++) {
				$content_attributes = $core->result->content[$j]->attributes();
				$publication_uuids[]  = (string)$content_attributes['uuid'];			
			}
		}	
		
		return $publication_uuids;
	}
	
	public static function get_pure_publications_batch($uuids) {
		global $wpdb, $client, $host, $username, $password;
		$num_uuids = count($uuids);
		if ($num_uuids == 0 ) return;
		
		$str_uuids = implode(",", $uuids);  
		$request = $client->get('/ws/rest/publication?rendering=xml_long&uuids.uuid='.$str_uuids.'&window.size='.$num_uuids);
		$request->setAuth($username, $password);		
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
		$core = $xml->children($namespaces['core']);
		$retrieved = date("F j, Y, g:i a"); 

		for ($i=0; $i < $core->count; $i++) {
			$p_core =  $core->result->content[$i]->children($namespaces['core']);
			$publication = $core->result->content[$i]->children($namespaces['publication-base_uk']);
			//$publication = $core->result->content[$i]->children($namespaces['stab']);
			$stab = $core->result->content[$i]->children('stab', true); 
			if ($publication->title == "") {
				$publication = $stab;
			}				

			$pub_attributes = $core->result->content[$i]->attributes();
			$pub_uuid = (string)$pub_attributes['uuid'];
			$portal_url = $p_core->portalUrl;
			// check if publication already exists in database. if so, don't need to add
			$table_name = $wpdb->prefix . "pure_profiles_publication";			
			$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE uuid='".$pub_uuid."'" );
			if (count($exists) > 0) {			
				continue; // exit current iteration only
			}
			
			$pub_title = $publication->title;
			$pub_subtitle = "";
			if ($publication->subtitle) {
				$pub_subtitle = $publication->subtitle;
			}
			$number_of_pages = $publication->numberOfPages;
			$abstract_str = "";
			if ($publication->abstract) {
				$abstract = $publication->abstract->children($namespaces['core']);
				$abstract_str = (string)$abstract->localizedString;
			}
			$bibliographical_note_str = "";
			if ($publication->bibliographicalNote) {
				$bibliographical_note = $publication->bibliographicalNote->children($namespaces['core']);
				$bibliographical_note_str = (string)$bibliographical_note->localizedString;
			} else {
				$bibliographical_note_str = "";
			}
			if ($publication->typeClassification) {
				$type_classification = $publication->typeClassification->children($namespaces['core']);
				$type_classification_str = (string)$type_classification->term->localizedString;
			} else {
				$type_classification_str = "";
			}
			$pub_year = "";
			$pub_month = "";
			$pub_day = "";			
			if ($publication->publicationDate) {
				$publication_date = $publication->publicationDate->children($namespaces['core']);			
				$pub_year = $publication_date->year;
				$pub_month = $publication_date->month;
				$pub_day = $publication_date->day;
			}
			$volume = $publication->volume;
			$journal_number = $publication->journalNumber;
			$journal_pages = $publication->pages;
			$type_classification = $publication->typeClassification->children($namespaces['core']);
			if ($publication->language) {
				$language = $publication->language->children($namespaces['core']);
				$language_str = (string)$language->term->localizedString;
			} else {
				$language_str = "";
			}
			if ($publication->startedWorkflows) {
				$workflows = $publication->startedWorkflows->children($namespaces['core']);
				$state = (string)$workflows->startedWorkflow->core->state;
			} else {
				$state = "";
			}
			if ($publication->dois) {
				$dois = $publication->dois->children($namespaces['core']);
				$dois_array = array();
				foreach ($dois->doi->doi as $d) {
					$dois_array[] = $d;
				}
				$dois_str = implode(",",$dois_array);
			} else {
				$dois_str = "";
			}
			$journal_title_str = "";
			$journal_issn_str = "";
			if ($publication->journal) {
				$journal = $publication->journal->children($namespaces['journal-template']);
				$journal_title = $journal->title->children($namespaces['extensions-core']);
				$journal_title_str = $journal_title->string;
				if ($journal->issn) {
					$journal_issn = $journal->issn->children($namespaces['extensions-core']);
					$journal_issn_str = $journal_issn->string;
				}
			}
						
			$peer_review = "false";
			if ($stab->peerReview) {
				$peer_review = $stab->peerReview;
			}
			
			$isbns = array();
			if ($stab->associatedPublisher) {
				$associated_publisher = $stab->associatedPublisher->children($namespaces['publisher-template']);		
				if ($associated_publisher->printIsbns) {
					$print_isbns = $associated_publisher->printIsbns->children($namespaces['extensions-core']);				
					$num_isbns = count($print_isbns->value);
					for ($j=0; $j < $num_isbns; $j++) {
						$isbn = $print_isbns->value[$j];
						$isbns[]  = (string)$isbn;			
					}
				}
			}
			$the_print_isbns = implode(",", $isbns);	
			
			$isbns = array();
			if ($stab->associatedPublisher) {
				$associated_publisher = $stab->associatedPublisher->children($namespaces['publisher-template']);		
				if ($associated_publisher->electronicIsbns) {
					$e_isbns = $associated_publisher->electronicIsbns->children($namespaces['extensions-core']);				
					$num_isbns = count($e_isbns->value);
					for ($j=0; $j < $num_isbns; $j++) {
						$isbn = $e_isbns->value[$j];
						$isbns[]  = (string)$isbn;			
					}
				}
			}
			$the_e_isbns = implode(",", $isbns);			
			
			/* get vancouver citation */
			$vancouver_str = "";
			$request = $client->get('/ws/rest/publication?rendering=vancouver&uuids.uuid='.$pub_uuid.'&window.size=1');
			$request->setAuth($username, $password);
			try {
				$van_response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}
			$van_xml = $van_response->xml();
			$van_core = $van_xml->children($namespaces['core']);
			$van_rendered = $van_core->result->renderedItem->children()->asXML();
 			$vancouver_str = $van_rendered;
			
			/* get harvard citation */
			$harvard_str = "";
			$request = $client->get('/ws/rest/publication?rendering=harvard&uuids.uuid='.$pub_uuid.'&window.size=1');
			$request->setAuth($username, $password);
			try {
				$hrv_response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}
			$hrv_xml = $hrv_response->xml();
			$hrv_core = $hrv_xml->children($namespaces['core']);
			$hrv_rendered = $hrv_core->result->renderedItem->children()->asXML();
 			$harvard_str = $hrv_rendered;			
											
			/* insert publications into table */
			$rows_affected = $wpdb->insert( $table_name, array( 
				'uuid' => $pub_uuid, 			
				'title' => $pub_title, 
				'subtitle' => $pub_subtitle,
				'portalurl' => $portal_url,
				'abstract' => $abstract_str,
				'typeclassification' => $type_classification_str,
				'originallanguage' => $language_str,
				'state' => $state,
				'dois' => $dois_str,
				'printisbns' => $the_print_isbns,
				'eisbns' => $the_e_isbns,
				'numberofpages' => $number_of_pages,
				'publicationyear' => $pub_year,
				'publicationmonth' => $pub_month, 
				'publicationday' => $pub_day, 
				'volume' => $volume,
				'journalnumber' => $journal_number,
				'journalpages' => $journal_pages,
				'journaltitle' => $journal_title_str,
				'journalissn' => $journal_issn_str,
				'bibliographicalnote' => $bibliographical_note_str,
				'vancouver' => $vancouver_str,
				'harvard' => $harvard_str,
				'peerreview' => $peer_review,
				'retrieved' => $retrieved
			));
			
			if ($publication->documents) {
				$table_name = $wpdb->prefix . "pure_profiles_publication_document";					
				// $documents = array();
				
				$doc_extensions = $publication->documents->children($namespaces['extensions-core']);
				foreach ($doc_extensions->document as $de) {
					$doc_core = $de->children($namespaces['core']);
					$doc_url = "http://www.research.ed.ac.uk/portal/files/".$doc_core->id."/".$doc_core->fileName;
					
					$rows_affected = $wpdb->insert( $table_name, array( 
							'publicationuuid' => $pub_uuid, 			
							'url' => $doc_url,
							'retrieved' => $retrieved
						)); 					
				}
				
			}

			if ($p_core->keywordGroups) {
				$table_name = $wpdb->prefix . "pure_profiles_publication_keywords";			
				$keywordGroups = $p_core->keywordGroups->children( $namespaces['core'] );
				$keywordGroupsCount = count($keywordGroups);
				for ($kg=0; $kg < $keywordGroupsCount; $kg++) {
								
					$keywordContainerAttributes = $keywordGroups[$kg]->keyword->attributes();
					$keywordContainerId = $keywordContainerAttributes->id;	
					
					$userDefinedKeywords = $keywordGroups[$kg]->keyword->children($namespaces['core']);
					$userDefinedKeywordsCount = count($userDefinedKeywords);
					for ($udk=0; $udk < $userDefinedKeywordsCount; $udk++) {
						$userDefinedKeywordAttributes = $userDefinedKeywords[$udk]->attributes();
						$userDefinedKeywordId = $userDefinedKeywordAttributes->id;
						$keywords = $userDefinedKeywords[$udk]->children($namespaces['core']);
						$keywordsCount = count($keywords);
						if ($keywordsCount ==1) {
							$keyword = $keywords->freeKeyword;
							/* insert publication keywords into table */
							$rows_affected = $wpdb->insert( $table_name, array( 
								'publicationuuid' => $pub_uuid, 			
								'keywordcontainerid' => $keywordContainerId, 
								'userdefinedkeywordid' => $userDefinedKeywordId,
								'keyword' => $keyword,
								'retrieved' => $retrieved
							)); 						
						} else {
							for ($kw=0; $kw < $keywordsCount; $kw++) {
								$keyword = $keywords->freeKeyword[$kw];
								/* insert publication keywords into table */
								$rows_affected = $wpdb->insert( $table_name, array( 
									'publicationuuid' => $pub_uuid, 			
									'keywordcontainerid' => $keywordContainerId, 
									'userdefinedkeywordid' => $userDefinedKeywordId,
									'keyword' => $keyword,
									'retrieved' => $retrieved
								)); 
							}
						}
					}
				}
			}
										
			if ($publication->persons) {
				$personAssociations = $publication->persons->children( $namespaces['person-template'] );
				$personAssociationsCount = count( $personAssociations );		
				for ($j=0; $j < $personAssociationsCount; $j++) {
				
					$associated_person_uuid = "";
					$first_name = "";
					$last_name = "";
					
					$person = $personAssociations[$j]->children($namespaces['person-template']);

					if ($person->person) {
						/* person is in Pure */
						$personAttributes = $person->person->attributes();
						$associated_person_uuid = $personAttributes->uuid;
					}
					
					$person_name = $person->name->children($namespaces['core']);
					$first_name = ($person_name->firstName) ? $person_name->firstName : ""; 
					$last_name = ($person_name->lastName) ? $person_name->lastName : "";	

					$person_role = $person->personRole->children($namespaces['core']);
					$person_role_str = $person_role->term->localizedString;
					
					// insert person associations into table 
					$table_name = $wpdb->prefix . "pure_profiles_publication_person";
					$rows_affected = $wpdb->insert( $table_name, array( 
						'publicationuuid' => $pub_uuid, 
						'title' => $pub_title, 
						'personuuid' => $associated_person_uuid,
						'firstname' => $first_name, 
						'lastname' => $last_name, 
						'personrole' => $person_role_str,
						'retrieved' => $retrieved
					));

				}
			}

			if ($publication->organisations) {
				$organisationAssociations = $publication->organisations->children($namespaces['organisation-template']);
				$organisationAssociationsCount = count($organisationAssociations);		
				for ($j=0; $j < $organisationAssociationsCount; $j++) {
				
					$organisation = $organisationAssociations[$j]->children($namespaces['organisation-template']);

					if ($organisation->organisation) {
						$organisation_id = ($organisation->organisationId) ? $organisation->organisationId : "";
						$organisationAttributes = $organisationAssociations[$j]->organisation->attributes();
						$associated_organisation_uuid = $organisationAttributes->uuid;
						if ($organisation->organisation->name) {					
							$organisation_name = $organisation->organisation->name->children($namespaces['core']);
							$name = ($organisation_name->localizedString) ? (string)$organisation_name->localizedString : ""; 
						} else {
							$name = "";
						}
						if ($organisation->organisation->shortName) {					 
							$organisation_short_name = $organisation->organisation->shortName->children($namespaces['core']);
							$short_name = ($organisation_short_name->localizedString) ? (string)$organisation_short_name->localizedString : ""; 
						} else {
							$short_name = "";
						}
						if ($organisation->organisation->typeClassification) {
							$organisation_type_classification_attributes = $organisation->organisation->typeClassification->attributes();
							$org_type_classification_id = (string)$organisation_type_classification_attributes->id;
							$organisation_type_classification = $organisation->organisation->typeClassification->children($namespaces['core']);
							$org_type_classification = (string)$organisation_type_classification->term->localizedString;
						} else {
							$org_type_classification_id = "";
							$org_type_classification = "";
						}
					} else {
						if ($organisation->name) {
							$organisationAttributes = $organisationAssociations[$j]->attributes();
							$associated_organisation_uuid = $organisationAttributes->uuid;							
							$organisation_name = $organisation->name->children($namespaces['core']);
							$name = ($organisation_name->localizedString) ? $organisation_name->localizedString : ""; 
							$organisation_id = ($organisation->organisationId) ? $organisation->organisationId : "";
							$organisation_short_name = $organisation->shortName->children($namespaces['core']);
							$short_name = ($organisation_short_name->localizedString) ? $organisation_short_name->localizedString : "";
							$organisation_type_classification_attributes = $organisation->typeClassification->attributes();
							$org_type_classification_id = $organisation_type_classification_attributes->id;
							$organisation_type_classification = $organisation->typeClassification->children($namespaces['core']);
							$org_type_classification = $organisation_type_classification->term->localizedString;
						}		
					}
				}
				
				// insert organisation associations into table 
				$table_name = $wpdb->prefix . "pure_profiles_publication_organisation";
				$rows_affected = $wpdb->insert( $table_name, array( 
					'publicationuuid' => $pub_uuid, 
					'title' => $pub_title, 
					'organisationuuid' => $associated_organisation_uuid,
					'organisationid' => $organisation_id,
					'organisationname' => $name, 
					'organisationshortname' => $short_name,
					'orgtypeclassification' => $org_type_classification, 
					'orgtypeclassificationid' => $org_type_classification_id,
					'retrieved' => $retrieved
				));

			}			
			
			
			if ($stab->associatedProjects) {
				$projectAssociations = $stab->associatedProjects->children($namespaces['project-template']);
				$projectAssociationsCount = count($projectAssociations);				
				for ($p=0; $p < $projectAssociationsCount; $p++) {

					$project = $projectAssociations[$p]->children($namespaces['project-template']);

					if ($project->project) {
						$proj_attributes = $projectAssociations[$p]->project->attributes();
						$proj_uuid = $proj_attributes->uuid;
						if ($project->project->title) {					
							$project_title = $project->project->title->children($namespaces['core']);
							$proj_title = ($project_title->localizedString) ? $project_title->localizedString : ""; 
						}			
					} else {
						if ($project->title) {
							$proj_attributes = $projectAssociations[$p]->attributes();
							$proj_uuid = $proj_attributes->uuid;							
							$project_title = $project->title->children($namespaces['core']);
							$proj_title = ($project_title->localizedString) ? $project_title->localizedString : ""; 
						}		
					}									
					// insert project associations into table 
					$table_name = $wpdb->prefix . "pure_profiles_project_publication_association";
					$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE project_uuid='".$proj_uuid."' AND publication_uuid='".$pub_uuid."'" );
					if (count($exists) > 0) {
						continue; // exit current iteration only
					}
					$rows_affected = $wpdb->insert( $table_name, array( 
						'project_uuid' => $proj_uuid,
						'project_title' => $proj_title, 					
						'publication_uuid' => $pub_uuid, 
						'publication_title' => $pub_title, 
						'retrieved' => $retrieved
					));					
	 				
				}
			}	
			

			if ($stab->associatedPublications) {
			echo "got to here";
				$publicationAssociations = $stab->associatedpublications->children($namespaces['publication-base_uk']);
				$publicationAssociationsCount = count($publicationAssociations);	
				for ($j=0; $j < $publicationAssociationsCount; $j++) {
				
					$ass_publication = $publicationAssociations[$j]->children($namespaces['publication-base_uk']);

					if ($ass_publication->publication) {
					echo " -1- ";
						$ass_publication_attributes = $publicationAssociations[$j]->publication->attributes();
						$associated_publication_uuid = $ass_publication_attributes->uuid;
						if ($ass_publication->publication->title) {					
							$ass_publication_title = $ass_publication->publication->title->children($namespaces['core']);
							$associated_publication_title = ($ass_publication_title->localizedString) ? $ass_publication_title->localizedString : ""; 
						}			
					} else {
					echo " -2- ";
						if ($ass_publication->title) {
							$ass_publication_attributes = $publicationAssociations[$j]->attributes();
							$associated_publication_uuid = $ass_publication_attributes->uuid;							
							$ass_publication_title = $ass_publication->title->children($namespaces['core']);
							$associated_publication_title = ($ass_publication_title->localizedString) ? $ass_publication_title->localizedString : ""; 
						}		
					}

					
					// insert publication associations into table 
					$table_name = $wpdb->prefix . "pure_profiles_publication_publication_association";
					$rows_affected = $wpdb->insert( $table_name, array( 
						'pub1_uuid' => $pub_uuid,
						'pub1_title' => $pub_title, 					
						'pub2_uuid' => $associated_publication_uuid, 
						'pub2_title' => $associated_publication_title, 
						'retrieved' => $retrieved
					));
				}
			}	
			
			
			/**
			
							if ($stab->associatedPublications) {
					for ($p=0; $p < $stab->associatedPublications->publication->count(); $p++) {
					
						$pub_attributes = $stab->associatedPublications->publication[$p]->attributes();
						$pub_uuid = (string)$pub_attributes['uuid'];
						$pub_title = (string)$stab->associatedPublications->publication[$p]->title;
									
						// insert publication associations into table 
						$table_name = $wpdb->prefix . "pure_profiles_project_publication_association";
						$exists = $wpdb->get_results( "SELECT * FROM ".$table_name." WHERE project_uuid='".$proj_uuid."' AND publication_uuid='".$pub_uuid."'" );
						if (count($exists) > 0) {			
							continue; // exit current iteration only
						}
						
						$rows_affected = $wpdb->insert( $table_name, array( 
							'project_uuid' => $proj_uuid,
							'project_title' => $proj_title, 					
							'publication_uuid' => $pub_uuid, 
							'publication_title' => $pub_title, 
							'retrieved' => $retrieved
						));					
					
					}
				}	
				
				**/
		
		}
	}
}	







