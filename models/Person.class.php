<?php if(!defined('PURECLIENT_PATH')){die('Direct access not permitted');}
require_once(PURECLIENT_PATH.'/libraries/guzzle.phar');
use Guzzle\Http\Client;
	
$connection_options = get_option('pure_profiles_connection_options');
if ($connection_options == NULL) return;
$host = $connection_options['pure_profiles_host_settings'];
$username = $connection_options['pure_profiles_username_settings'];
$password = $connection_options['pure_profiles_password_settings'];
$client = new Client($host, array(
	'request.options' => array(
		'auth'    => array($username, $password, 'Basic|Digest|NTLM|Any'),

	),
	'curl.options' => array(
		'curl.CURLOPT_SSL_VERIFYPEER' => false,
		'curl.CURLOPT_CERTINFO'       => false	
	),
	'ssl.certificate_authority' => false
));
	
class Person {

	public function __construct() {
    }

	public static function get_pure_persons() {
					
		$person_uuids = Person::get_pure_persons_uuids(); // get array of person UUIDs
		if (count($person_uuids) == 0 ) return;
			
		/* get persons by UUID, in batches of 20 */
		$batch = array();		
		$num_persons = count($person_uuids);
		for ($i=0; $i< $num_persons; $i++ ) {
			if (trim($person_uuids[$i]) != "") {
				$batch[] = $person_uuids[$i];
			}
		
			if (($i>0) && ((($i+1) % 20) == 0) ) { 

				if ($i < 21) {
					$clear_database = 1;
				} else {
					$clear_database = 0;
				}
				// make call
				Person::get_pure_persons_batch($batch, $clear_database);
				
				// make a new batch
				$batch = array();
			}
				
		}
		/* do remaining batch */
		if (count($batch) > 0) {
			if ($num_persons < 20) {
				$clear_database = 1;
			} else {
				$clear_database = 0;
			}
			Person::get_pure_persons_batch($batch, $clear_database);
		}
	}
	
	public static function get_pure_persons_uuids() {
		global $wpdb, $client, $username, $password;
		$parameters_options = get_option('pure_profiles_parameters_options');
		$employee_ids = preg_replace('/\s+/', '', trim($parameters_options['pure_profiles_person_ids_settings'])); // remove all whitespace
		$str_person_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_profiles_person_uuids_settings'])); // remove all whitespace	
		$org_uuids = preg_replace('/\s+/', '', trim($parameters_options['pure_profiles_org_uuids_settings'])); // remove all whitespace
		$person_uuids = explode(",", $str_person_uuids); // our main array

		/* get person UUIDs from organisation UUID */
		if ($org_uuids != "") {
		
			/* first of all, need to get the count */
			$request = $client->get('/ws/rest/person?rendering=xml_short&associatedOrganisationUuids.uuid='.$org_uuids.'&window.size=1');
			$request->setAuth($username, $password);
			try {
				$response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}
				
			$xml = $response->xml();
			$namespaces = $xml->getNamespaces(true);
			$core = $xml->children($namespaces['core']);	
			$result_count = $core->count;
			if ($result_count == 0) return;

			/* get person UUIDs, in batches of 20 */
			$batch_size = 20;
			for ($i =0; $i < $result_count; $i = $i+$batch_size) {
			
				$request = $client->get('/ws/rest/person?rendering=xml_short&associatedOrganisationUuids.uuid='.$org_uuids.'&window.size='.$batch_size.'&window.offset='.$i);
				$request->setAuth($username, $password);
				try {
					$response = $request->send();
				} catch (Guzzle\Http\Exception\BadResponseException $e) {
					echo $e->getMessage();
				}
					
				$xml = $response->xml();
				$namespaces = $xml->getNamespaces(true);
				$core = $xml->children($namespaces['core']);	
				$num_batch_results = count($core->result->content);
				for ($j=0; $j < $num_batch_results; $j++) {
					$content_attributes = $core->result->content[$j]->attributes();
					$person_uuids[]  = (string)$content_attributes['uuid'];			
				}
			}		
		}
		
		/* get person UUIDs from employee ids */
		if ($employee_ids != "") {
			$request = $client->get('/ws/rest/person?rendering=xml_short&personEmployeeIds.value='.$employee_ids.'&window.size=1000');
			$request->setAuth($username, $password);
			try {
				$response = $request->send();
			} catch (Guzzle\Http\Exception\BadResponseException $e) {
				echo $e->getMessage();
			}				
			$xml = $response->xml();
			$namespaces = $xml->getNamespaces(true);
			$core = $xml->children($namespaces['core']);				
			for ($i=0; $i < $core->count; $i++) {
				$content_attributes = $core->result->content[$i]->attributes();
				$person_uuids[]  = (string)$content_attributes['uuid'];				
			}
		}
				
		/* ensure person UUIDs are unique */
		if (count($person_uuids) > 1) {
			$p_uuids_unique = array_unique($person_uuids);
		} else {
			$p_uuids_unique = $person_uuids;
		}
		/* remove any empty entries */
		$p_uuids_unique = array_filter($p_uuids_unique);
		
		return array_values($p_uuids_unique); // ensuring array keys start at zero
	}
	
	public static function get_pure_persons_batch($uuids, $clear_database=0) {
		global $wpdb, $client, $host, $username, $password;
		$num_uuids = count($uuids);
		if ($num_uuids == 0 ) return;

		$str_uuids = implode(",", $uuids);  // should limit this to 20 persons and/or URL string length (2000 characters)?
		$request = $client->get('/ws/rest/person?rendering=xml_long&uuids.uuid='.$str_uuids.'&window.size='.$num_uuids);
		$request->setAuth($username, $password);
		try {
			$response = $request->send();
		} catch (Guzzle\Http\Exception\BadResponseException $e) {
			echo $e->getMessage();
		}		
		$xml = $response->xml();
		$namespaces = $xml->getNamespaces(true);
		
		$core = $xml->children($namespaces['core']);
				
		/* if query is successful, and flag is set, clear out existing entries */
		if (($core->count > 0) && ($clear_database == 1) ) {
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_person";
			$wpdb->query($truncate_sql);
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_person_information";
			$wpdb->query($truncate_sql);
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_organisation_association";
			$wpdb->query($truncate_sql);
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_staff_organisation_association";
			$wpdb->query($truncate_sql);			
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project";
			$wpdb->query($truncate_sql);
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_keywords";
			$wpdb->query($truncate_sql);			
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_person";
			$wpdb->query($truncate_sql);

			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_organisation";
			$wpdb->query($truncate_sql);			
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_project_publication_association";
			$wpdb->query($truncate_sql);
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication";
			$wpdb->query($truncate_sql);	

			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_keywords";
			$wpdb->query($truncate_sql);				

			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_person";
			$wpdb->query($truncate_sql);	
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_publication_organisation";
			$wpdb->query($truncate_sql);			
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping";
			$wpdb->query($truncate_sql);				

			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping_reference";
			$wpdb->query($truncate_sql);				
			
			$truncate_sql = "TRUNCATE TABLE ".$wpdb->prefix . "pure_profiles_clipping_person";
			$wpdb->query($truncate_sql);				
			
		} 
		
		$retrieved = date("F j, Y, g:i a"); 
		for ($i=0; $i < $core->count; $i++) {
			$person = $core->result->content[$i]->children($namespaces['stab1']);
			$p_core = $core->result->content[$i]->children($namespaces['core']);
			$attributes = $core->result->content[$i]->attributes();
			$uuid = (string)$attributes['uuid'];	
			$employee_id = (string)$person->employeeId;
			$portal_url = $p_core->portalUrl;
			$name = $person->name->children($namespaces['core']);
			$call_name_first_name = "";
			$call_name_last_name = "";			
			if ($person->callName) {
				$call_name = $person->callName->children($namespaces['core']);
				$call_name_first_name = $call_name->firstName;
				$call_name_last_name = $call_name->lastName;
			}
			$stab = $core->result->content[$i]->children($namespaces['stab']);
			$title = $stab->title;
						
			/* get all photos */
			$photos_str = "";		
			if ( ($host != "https://www-test.pure.ed.ac.uk") && ($host != "http://www-test.pure.ed.ac.uk") ){ // discrepancies can break script, so just ignore images when accessing Test	
				set_time_limit(600); // otherwise PHP will time out for larger photographs
				$photos_arr = array();
				if ($person->photos) {
					$photos = $person->photos->children($namespaces['core']);
					foreach ($photos->file as $phot) {
						$phot_client = new Client($host, array(
							'request.options' => array(
								'auth'    => array($username, $password, 'Basic|Digest|NTLM|Any'),

							),
							'curl.options' => array(
								'curl.CURLOPT_SSL_VERIFYPEER' => false,
								'curl.CURLOPT_CERTINFO'       => false	
							),
							'ssl.certificate_authority' => false
						));				
						$filename = substr( $phot->url, strrpos( $phot->url, '/' )+1 );
						@mkdir(PURECLIENT_PATH.'files/'.$employee_id, 0755);
						$new_file_url = 'files/'.$employee_id.'/'.$filename;
						$new_file_path = PURECLIENT_PATH.$new_file_url;
						try {					
							$phot_response = $phot_client->get($phot->url)->setAuth($username, $password)->setResponseBody($new_file_path)->send();
							chmod($new_file_path, 0744); 
						} catch (Guzzle\Http\Exception\BadResponseException $e) {
							echo $e->getMessage();
						}
						
						$photos_arr[] = $new_file_url;
					}
					$photos_str = implode(",", $photos_arr);
				}							
			} // end if ($host != "https://www-test.pure.ed.ac.uk")

			/* insert persons into table */
			$table_name = $wpdb->prefix . "pure_profiles_person";
			$rows_affected = $wpdb->insert( $table_name, array( 
				'uuid' => $uuid, 
				'employeeid' => $employee_id, 				
				'portalurl' => $portal_url,
				'firstname' => $name->firstName, 
				'lastname' => $name->lastName, 
				'callnamefirstname' => $call_name_first_name,
				'callnamelastname' => $call_name_last_name,
				'title' => $title, 
				'photos' => $photos_str,
				'retrieved' => $retrieved
			));
																						
			/* get all profile information */
			if ($person->profileInformation) {
				$profile_information = $person->profileInformation->children($namespaces['extensions-core']);
				$num_profile_information = count($profile_information->customField);
				for ($pi=0; $pi < $num_profile_information; $pi++) {
					$info_title = $profile_information->customField[$pi]->typeClassification->children($namespaces['core']);
					$the_info_title = $info_title->term->localizedString;
					$info_content = $profile_information->customField[$pi]->value->children($namespaces['core']);
					$the_info_content = ($info_content->localizedString) ? $info_content->localizedString : "";
					
					
					/* insert profile information into table */
					$table_name = $wpdb->prefix . "pure_profiles_person_information";
					$rows_affected = $wpdb->insert( $table_name, array( 
						'personuuid' => $uuid, 
						'title' => $the_info_title, 
						'content' => $the_info_content, 
						'retrieved' => $retrieved
					));				
				}
			}

											
			/* get all organisation associations*/
			$organisation_associations = $person->organisationAssociations;
			$num_organisations = count($organisation_associations);
			
			for ($j=0; $j < $num_organisations; $j++) {
				$org_person = $person->organisationAssociations->organisationAssociation[$j]->children($namespaces['stab1']);
				$employment_type = $org_person->employmentType->children($namespaces['core']);
				$employement_type_attributes = $person->organisationAssociations->organisationAssociation[$j]->employmentType->attributes();
				$employment_type_id = (string)$employement_type_attributes['id'];
				$organisation = $person->organisationAssociations->organisationAssociation[$j]->organisation->children($namespaces['organisation-template']);
				$organisation_name = (string)$organisation->name->children($namespaces['core'])->localizedString;
				if ($organisation->shortName) {
					$organisation_short_name = $organisation->shortName->children($namespaces['core']);
				} else {
					$organisation_short_name = "";
				}
				$org_attributes = $person->organisationAssociations->organisationAssociation[$j]->organisation->attributes();
				$org_uuid = (string)$org_attributes->uuid;	
				if	($organisation->typeClassification) {
					$org_type_classification_attributes = $organisation->typeClassification->attributes();
					$org_type_classification_id = (string)$org_type_classification_attributes->id;
					$org_type_classification = $organisation->typeClassification->children($namespaces['core']);
					$org_type_classification_str = (string)$org_type_classification->term->localizedString;
				} else {
					$org_type_classification_id = "";
					$org_type_classification_str = "";
				}
		
				/* insert organisation associations into table */
				$table_name = $wpdb->prefix . "pure_profiles_organisation_association";
				$rows_affected = $wpdb->insert( $table_name, array( 
					'personuuid' => $uuid, 
					'employeeid' => $employee_id, 
					'organisationuuid' => $org_uuid, 
					'organisationid' => $organisation->organisationId, 
					'organisationname' => $organisation_name, 
					'organisationshortname' => $organisation_short_name, 
					'organisationtypeclassificationid' => $org_type_classification_id, 
					'organisationtypeclassification' => $org_type_classification_str, 
					'employmenttype' => $employment_type->term->localizedString, 
					'employmenttypeid' => $employment_type_id, 
					'retrieved' => $retrieved
				));

			}
			
			/* get all staff organisation associations*/
			$staff_organisation_associations = $stab->staffOrganisationAssociations;
			$num_staff_organisations = count($staff_organisation_associations);
			for ($k=0; $k < $num_staff_organisations; $k++) {
				$staff_org_stab = $stab->staffOrganisationAssociations->staffOrganisationAssociation[$k]->children($namespaces['stab']);
				$job_description = $staff_org_stab->jobDescription;
				$staff_org_person = $stab->staffOrganisationAssociations->staffOrganisationAssociation[$k]->children($namespaces['stab1']);
				$staff_org_person_org_attributes = $staff_org_person->organisation->attributes();
				$staff_org_uuid = (string)$staff_org_person_org_attributes['uuid'];
				
				/* insert staff organisation associations into table */
				$table_name = $wpdb->prefix . "pure_profiles_staff_organisation_association";
				$rows_affected = $wpdb->insert( $table_name, array( 
					'personuuid' => $uuid, 
					'employeeid' => $employee_id, 
					'organisationuuid' => $staff_org_uuid, 
					'jobdescription' => $job_description, 
					'retrieved' => $retrieved
				));				
			}
			
			Publication::get_publications("Person", $uuid);
			Project::get_projects("Person", $uuid);
			
			Clipping::get_clippings("Person", $uuid);
			
		}
		
	}
		
}