<?php
/*
 * This script should be called overnight from a cron. It could also be called by pressing a button in the control panel.
 */
if ( !defined('ABSPATH') ) {
	/** Set up WordPress environment */
	require_once( '../../../wp-load.php');
}
//define('PURECLIENT_PATH', plugin_dir_path(__FILE__) );
require_once(PURECLIENT_PATH.'/libraries/guzzle.phar');
require_once(PURECLIENT_PATH.'/models/Clipping.class.php');
require_once(PURECLIENT_PATH.'/models/Person.class.php');
require_once(PURECLIENT_PATH.'/models/Project.class.php');
require_once(PURECLIENT_PATH.'/models/Publication.class.php');
require_once(PURECLIENT_PATH.'/models/Organisation.class.php');

// check that the key defined in options matches the one in the query string
$qs_key = $_REQUEST['key'];
$cron_options = get_option('pure_profiles_cron_options');
$options_key = $cron_options['pure_profiles_cron_key_settings'];

if ($qs_key == $options_key) {

	// call update functions
	Person::get_pure_persons();
	Organisation::get_pure_organisations(1);
	echo "Finished updating local cache";
} else {
	echo "Keys do not match";
}
