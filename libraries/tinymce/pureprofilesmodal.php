<?php
require_once '../../../../../wp-load.php';
global $wpdb;

$persons_for_select = $wpdb->get_results("SELECT uuid, title, callnamefirstname, lastname from " . $wpdb->prefix . "pure_profiles_person ORDER BY lastname ASC");

$persons_select = array();
foreach($persons_for_select as $pfs) {
	$pfs->title = ucfirst(strtolower($pfs->title));
	$persons_select[$pfs->uuid] = $pfs->title." ".$pfs->callnamefirstname." ".$pfs->lastname;
}

?>

<!DOCTYPE html>
<head>
	<title>Create Profile Shortcode</title>
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script type="text/javascript" src="../../../../../wp-includes/js/tinymce/tiny_mce_popup.js"></script>
	<script type="text/javascript">
	var ProfileTiny = {
		e: '',
		init: function(e) {
			ProfileTiny.e = e;
			tinyMCEPopup.resizeToInnerSize();
		},
		insert: function createProfileShortcode(e) {
			//Create Shortcode
			var personUUID = $('#personuuid').val();
			var hide_profileinformation = $('#hide_profileinformation').is(':checked') ? $('#hide_profileinformation').val() : '';
			var hide_publications = $('#hide_publications').is(':checked') ? $('#hide_publications').val() : '';
			var hide_projects = $('#hide_projects').is(':checked') ? $('#hide_projects').val() : '';
			var hide_clippings = $('#hide_clippings').is(':checked') ? $('#hide_clippings').val() : '';
			var display_all_info = $('#display_all_info').is(':checked') ? 'yes' : 'no';
			
			var output = '[pureprofiles';
			if(personUUID) {
				output += ' personuuid="'+personUUID+'"';
			}
			if(hide_profileinformation || hide_publications || hide_projects || hide_clippings) {
				output += ' hide="'+hide_profileinformation+hide_publications+hide_projects+hide_clippings+'"';
			}		
			if(display_all_info == 'yes') {
				output += ' display_all_info="yes"';
			}
			output += ']';
			
			tinyMCEPopup.execCommand('mceReplaceContent', false, output);
			
			tinyMCEPopup.close();
			
		}
	}
	tinyMCEPopup.onInit.add(ProfileTiny.init, ProfileTiny);
	</script>
</head>
<body>
<form id="ProfileShortcode">
	<p>
		<label for="personuuid">Person to display:</label><br/>
		<select name="personuuid" id="personuuid">
			<option value="">-display all-</option>
		<?php foreach ($persons_select as $id=>$ps) { ?>
			<option value="<?php echo $id; ?>"><?php echo $ps; ?></option>
		<?php } ?>
		</select><br/><br/>
		
		<label for="sectionstohide">Sections to hide:</label><br/>
		<input type="checkbox" name="hide_group[]" id="hide_profileinformation" value="profileinformation " />Profile Information<br />
		<input type="checkbox" name="hide_group[]" id="hide_publications" value="publications " />Publications<br />
		<input type="checkbox" name="hide_group[]" id="hide_projects" value="projects " />Projects<br />
		<input type="checkbox" name="hide_group[]" id="hide_clippings" value="clippings" />Research Press Coverage<br />	
		<br/>
		<label for="display_all_info">Display detailed information for Publications and Projects? </label>
		<input type="checkbox" name="display_all_info" id="display_all_info" value="yes" /><br/>
		
	</p>

	<p><button onclick="javascript:ProfileTiny.insert(ProfileTiny.e)">Create Shortcode</button></p>
</form>
</body>
</html>
