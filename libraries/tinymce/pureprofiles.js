(function() {
	tinymce.create('tinymce.plugins.ExamplePlugin', {
		init : function(ed, url) {
			ed.addCommand('tinyExample', function() {
				ed.windowManager.open({
					file : url + '/pureprofilesmodal.php',
					width : 450 + parseInt(ed.getLang('example.delta_width', 0)),
					height : 450 + parseInt(ed.getLang('example.delta_height', 0)),
					inline : 1
				}, {
					plugin_url : url
				});
			});
			ed.addButton('pure_profiles_button', {title : 'Add Pure Profile', cmd : 'tinyExample', icon : 'code' });
		},
		getInfo : function() {
			return {
				longname : 'Pure Profiles',
				author : 'Gavin Maxwell',
				authorurl : 'http://www.ed.ac.uk',
				infourl : 'http://www.ed.ac.uk',
				version : tinymce.majorVersion + "." + tinymce.minorVersion
			};
		}
	});
	tinymce.PluginManager.add('pure_profiles_button', tinymce.plugins.ExamplePlugin);
})();
